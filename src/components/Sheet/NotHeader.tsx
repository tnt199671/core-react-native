import React, { useRef, memo, useState, useCallback, useMemo } from 'react';
import { View } from 'react-native';
import ActionSheet, {
  registerSheet,
  SheetProps,
  SheetManager
} from 'react-native-actions-sheet';
import { SHEET_TYPE, SHEET } from '@constants';

type DataProps = {
  type: SHEET_TYPE;
  data?: any;
  callback: (value: any, value2?: any) => void | undefined;
};

function Sheet({ sheetId }: SheetProps) {
  const actionSheetRef = useRef(null);
  const [state, setState] = useState({
    type: SHEET_TYPE.DEFAULT,
    data: null,
    callback: (value: any, value2?: any) => {}
  });

  const onBeforeShow = useCallback((data: DataProps) => {
    setState({
      ...state,
      ...data
    })
  }, [state]);

  const onClose = useCallback(() => {
    setState({
      type: SHEET_TYPE.DEFAULT,
      data: null,
      callback: () => { }
    })
  }, []);

  const onHide = useCallback(() => {
    SheetManager.hide(SHEET.NOT_HEADER);
  }, []);

  const renderContent = useMemo(() => {
    switch (state.type) {
      case SHEET_TYPE.ANY:
        return <View />;
      default:
        return <View />
    }
  }, [state.type]);

  return (
    <ActionSheet
      onBeforeShow={onBeforeShow as (data?: any) => void}
      id={sheetId}
      ref={actionSheetRef}
      drawUnderStatusBar={false}
      gestureEnabled={false}
      defaultOverlayOpacity={0.7}
      closeOnTouchBackdrop={true}
      headerAlwaysVisible={false}
      keyboardHandlerEnabled={false}
      onClose={onClose}
    >
      {renderContent}
    </ActionSheet>
  );
}

export default memo(Sheet);

registerSheet(SHEET.NOT_HEADER, Sheet);