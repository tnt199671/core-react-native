import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { scaleSizeWidth } from '@utils/Scaler';
import { AppInputSearch } from '@components';

const BlockInputSearch = () => {
  const [search, setSearch] = useState('');

  return (
    <AppInputSearch
      placeholder={'tìm kiếm...'}
      onChange={setSearch}
      style={styles.container}
      time={300}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: scaleSizeWidth(10),
  }
});

export default React.memo(BlockInputSearch);
