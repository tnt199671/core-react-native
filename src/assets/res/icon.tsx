import React from 'react';
import { scaleSizeWidth, scaleSizeHeight } from '@utils/Scaler';
import {Colors} from '@assets';
import MainHome from '../icons/Home.svg';
import MainHomeActive from '../icons/HomeActive.svg';
import MainGift from '../icons/Gift.svg';
import MainScan from '../icons/Scan.svg';
import MainScanVoucher from '../icons/ScanVoucher.svg';
import MainGiftActive from '../icons/GiftActive.svg';
import MainTip from '../icons/Tip.svg';
import MainTipActive from '../icons/TipActive.svg';
import MainCart from '../icons/Cart.svg';
import MainCartActive from '../icons/CartActive.svg';
import MainVoucher from '../icons/VoucherTab.svg';
import MainVoucherActive from '../icons/VoucherTabActive.svg';
import MainUser from '../icons/User.svg';
import MainUserActive from '../icons/UserActive.svg';
import CircleSuccess from '../icons/CircleSuccess.svg';
import CircleClose from '../icons/CircleClose.svg';
import CircleWarning from '../icons/CircleWarning.svg';

import EyesOpen from '../icons/EyesOpen.svg';
import EyesClose from '../icons/EyesClose.svg';
import EyesRed from '../icons/EyeRed.svg';
import DropDown from '../icons/DropDown.svg';
import DropDownRed from '../icons/DropDownRed.svg';
import DropDownWhite from '../icons/DropDownWhite.svg';
import Back from '../icons/Back.svg';
import BackWhite from '../icons/BackWhite.svg';
import Tick from '../icons/Tick.svg';
import TickWhite from '../icons/TickWhite.svg';
import Ticked from '../icons/Ticked.svg';
import TickedCircle from '../icons/TickedCircle.svg';
import TickedCircleOrg from '../icons/TickedCircleOrg.svg';
import TickCircleOutLine from '../icons/TickCircleOutLine.svg';
import Order from '../icons/Order.svg';
import OrderHistory from '../icons/OrderHistory.svg';
import Purchase from '../icons/Purchase.svg';
import Voucher from '../icons/Voucher.svg';
import Promotion from '../icons/Promotion.svg';
import Notification from '../icons/Notification.svg';
import LineVertical from '../icons/LineVertical.svg';
import Crown from '../icons/Crown.svg';
import CrownGray from '../icons/CrownGray.svg';
import ArrowUp from '../icons/ArrowUp.svg';
import ArrowUpRed from '../icons/ArrowUpRed.svg';
import ArrowDown from '../icons/ArrowDown.svg';
import ArrowDownRed from '../icons/ArrowDownRed.svg';
import Right from '../icons/Right.svg';
import PetShop from '../icons/PetShop.svg';
import Clinic from '../icons/Clinic.svg';
import Pending from '../icons/Pending.svg';
import Close from '../icons/Close.svg';
import CloseGray from '../icons/CloseGray.svg';
import CloseBlack from '../icons/CloseBlack.svg';
import CloseBase from '../icons/CloseBase.svg';
import Dog from '../icons/Dog.svg';
import DogWhite from '../icons/DogWhite.svg';
import DogBigSize from '../icons/DogBigSize.svg';
import Cat from '../icons/Cat.svg';
import CatWhite from '../icons/CatWhite.svg';
import CatBigSize from '../icons/CatBigSize.svg';
import Radio from '../icons/Radio.svg';
import RadioChecked from '../icons/RadioChecked.svg';
import Library from '../icons/Library.svg';
import Camera from '../icons/Camera.svg';
import CameraRed from '../icons/CameraRed.svg';
import CameraFrame from '../icons/CameraFrame.svg';
import PhoneGray from '../icons/PhoneGray.svg';
import PhoneRed from '../icons/PhoneRed.svg';
import PhoneOutlined from '../icons/PhoneOutlined.svg';
import Location from '../icons/Location.svg';
import LocationGray from '../icons/LocationGray.svg';
import LocationCircle from '../icons/LocationCircle.svg';
import RateMain from '../icons/RateMain.svg';
import Star from '../icons/Star.svg';
import StarEmpty from '../icons/StarEmpty.svg';
import Message from '../icons/Message.svg';
import Pen from '../icons/Pen.svg';
import PenWhite from '../icons/PenWhite.svg';
import Search from '../icons/Search.svg';
import SearchWhite from '../icons/SearchWhite.svg';
import Heart from '../icons/Heart.svg';
import HeartActive from '../icons/HeartActive.svg';
import HeartWhite from '../icons/HeartWhite.svg';
import CartWhite from '../icons/CartWhite.svg';
import Next from '../icons/Next.svg';
import Previous from '../icons/Previous.svg';
import Plus from '../icons/Plus.svg';
import ShopRank from '../icons/ShopRank.svg';
import Beautify from '../icons/Beautify.svg';
import Hotel from '../icons/Hotel.svg';
import Delivery from '../icons/Delivery.svg';
import Global from '../icons/Global.svg';
import Facebook from '../icons/Facebook.svg';
import Shopee from '../icons/Shopee.svg';
import Lazada from '../icons/Lazada.svg';
import Tiki from '../icons/Tiki.svg';
import LocationMap from '../icons/LocationMap.svg';
import RequestLocation from '../icons/RequestLocation.svg';
import Gold from '../icons/Gold.svg';
import Direction from '../icons/Direction.svg';
import ScanRed from '../icons/ScanRed.svg';
import FilterOutline from '../icons/FilterOutline.svg';

export default {
  MAIN_HOME: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive) {
      return <MainHomeActive width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />
    }
    return <MainHome width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MAIN_GIFT: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive) {
      return <MainGiftActive width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />
    }
    return <MainGift width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MAIN_SCAN: ({size = 24, fill='none'}: IconProps) => {
    return <MainScan width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MAIN_SCAN_VOUCHER: ({size = 24, fill='none'}: IconProps) => {
    return <MainScanVoucher width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MAIN_TIP: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive) {
      return <MainTipActive width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />
    }
    return <MainTip width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MAIN_CART: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive) {
      return <MainCartActive width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />
    }
    return <MainCart width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MAIN_VOUCHER: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive) {
      return <MainVoucherActive width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />
    }
    return <MainVoucher width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MAIN_USER: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive) {
      return <MainUserActive width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />
    }
    return <MainUser width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  EYES: ({width = 24, height = 19, fill='none', isOpen = true}: IconProps) => {
    if(isOpen){
      return <EyesOpen width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
    }
    return <EyesClose width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  EYES_RED: ({width = 16, height = 19, fill='none', isOpen = true}: IconProps) => {
    if(isOpen){
      return <EyesRed width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
    }
    return <EyesClose width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  DROP_DOWN: ({size = 30, fill='none'}: IconProps) => {
    return <DropDown width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  DROP_DOWN_RED: ({size = 16, fill='none'}: IconProps) => {
    return <DropDownRed width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  DROP_DOWN_WHITE: ({size = 16, fill='none'}: IconProps) => {
    return <DropDownWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  BACK: ({size = 30, fill='none'}: IconProps) => {
    return <Back width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  BACK_WHITE: ({size = 30, fill='none'}: IconProps) => {
    return <BackWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  TICK: ({size = 24, fill='none', isTicked = false}: IconProps) => {
    if(isTicked){
      return <Ticked width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <Tick width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  TICK_WHITE: ({size = 8, fill='none'}: IconProps) => {
    return <TickWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  TICKED_CIRCLE: ({size = 30, fill='none'}: IconProps) => {
    return <TickedCircle width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  TICKED_CIRCLE_ORG: ({size = 30, fill='none'}: IconProps) => {
    return <TickedCircleOrg width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  TICK_CIRCLE_OUT_LINE: ({size = 19, fill='none'}: IconProps) => {
    return <TickCircleOutLine width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CIRCLE_SUCCESS: ({size = 25, fill='none'}: IconProps) => {
    return <CircleSuccess width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CIRCLE_CLOSE: ({size = 25, fill='none'}: IconProps) => {
    return <CircleClose width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CIRCLE_WARNING: ({size = 25, fill='none'}: IconProps) => {
    return <CircleWarning width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  ORDER: ({size = 80, fill='none'}: IconProps) => {
    return <Order width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  ORDER_HISTORY: ({size = 40, fill='none'}: IconProps) => {
    return <OrderHistory width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PURCHASE: ({size = 80, fill='none'}: IconProps) => {
    return <Purchase width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  VOUCHER: ({size = 80, fill='none'}: IconProps) => {
    return <Voucher width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PROMOTION: ({size = 80, fill='none'}: IconProps) => {
    return <Promotion width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  NOTIFICATION: ({size = 80, fill='none'}: IconProps) => {
    return <Notification width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  LINE_VERTICAL: ({size = 22, fill='none'}: IconProps) => {
    return <LineVertical width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CROWN: ({width = 37, height = 21, fill='none'}: IconProps) => {
    return <Crown width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  CROWN_GRAY: ({width = 37, height = 21, fill='none'}: IconProps) => {
    return <CrownGray width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  ARROW_UP: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      return <ArrowUpRed width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <ArrowUp width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  ARROW_DOWN: ({size = 24, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      return <ArrowDownRed width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <ArrowDown width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  RIGHT: ({size = 18, fill='none'}: IconProps) => {
    return <Right width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PET_SHOP: ({size = 24, fill='none'}: IconProps) => {
    return <PetShop width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CLINIC: ({size = 24, fill='none'}: IconProps) => {
    return <Clinic width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PENDING: ({size = 24, fill='none'}: IconProps) => {
    return <Pending width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CLOSE: ({size = 24, fill='none'}: IconProps) => {
    return <Close width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CLOSE_GRAY: ({size = 20, fill='none'}: IconProps) => {
    return <CloseGray width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CLOSE_BLACK: ({size = 20, fill='none'}: IconProps) => {
    return <CloseBlack width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CLOSE_BASE: ({size = 30, fill='none'}: IconProps) => {
    return <CloseBase width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  DOG: ({size = 34, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      return <DogWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <Dog width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  DOG_BIG_SIZE: ({size = 100, fill='none'}: IconProps) => {
    return <DogBigSize width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CAT: ({size = 34, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      return <CatWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <Cat width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CAT_BIG_SIZE: ({size = 100, fill='none'}: IconProps) => {
    return <CatBigSize width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  RADIO: ({size = 18, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      return <RadioChecked width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <Radio width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  LIBRARY: ({size = 48, fill='none'}: IconProps) => {
    return <Library width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CAMERA: ({size = 48, fill='none'}: IconProps) => {
    return <Camera width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CAMERA_RED: ({size = 19, fill='none'}: IconProps) => {
    return <CameraRed width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CAMERA_FRAME: ({size = 320, fill='none'}: IconProps) => {
    return <CameraFrame width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PHONE_GRAY: ({size = 16, fill='none'}: IconProps) => {
    return <PhoneGray width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PHONE_RED: ({size = 48, fill='none'}: IconProps) => {
    return <PhoneRed width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PHONE_OUTLINED: ({size = 20, fill='none'}: IconProps) => {
    return <PhoneOutlined width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  LOCATION: ({width = 20, height = 20, fill='none'}: IconProps) => {
    return <Location width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  LOCATION_GRAY: ({width = 16, height = 18, fill='none'}: IconProps) => {
    return <LocationGray width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  LOCATION_CIRCLE: ({size = 36, fill='none'}: IconProps) => {
    return <LocationCircle width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  RATE_MAIN: ({width = 130, height = 154, fill='none'}) => {
    return <RateMain width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  STAR: ({size = 36, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      return <Star width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <StarEmpty width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  MESSAGE: ({size = 48, fill='none'}: IconProps) => {
    return <Message width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PEN: ({size = 16, fill='none'}: IconProps) => {
    return <Pen width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PEN_WHITE: ({size = 20, fill='none'}: IconProps) => {
    return <PenWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  SEARCH: ({size = 24, fill='none'}: IconProps) => {
    return <Search width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  SEARCH_WHITE: ({size = 16, fill='none'}: IconProps) => {
    return <SearchWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  HEART_WHITE: ({size = 20, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      fill = Colors.white;
    }
    return <HeartWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  HEART: ({size = 20, fill='none', isActive = false}: IconProps) => {
    if(isActive){
      return <HeartActive width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
    }
    return <Heart width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  CART_WHITE: ({size = 20, fill='none'}: IconProps) => {
    return <CartWhite width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  NEXT: ({size = 16, fill='none'}: IconProps) => {
    return <Next width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PREVIOUS: ({size = 16, fill='none'}: IconProps) => {
    return <Previous width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  PLUS: ({size = 24, fill='none'}: IconProps) => {
    return <Plus width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  SHOP_RANK: ({size = 20, fill='none'}: IconProps) => {
    return <ShopRank width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  BEAUTIFY: ({size = 20, fill='none'}: IconProps) => {
    return <Beautify width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  HOTEL: ({size = 20, fill='none'}: IconProps) => {
    return <Hotel width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  DELIVERY: ({size = 20, fill='none'}: IconProps) => {
    return <Delivery width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  GLOBAL: ({size = 20, fill='none'}: IconProps) => {
    return <Global width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  FACEBOOK: ({size = 20, fill='none'}: IconProps) => {
    return <Facebook width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  SHOPEE: ({size = 20, fill='none'}: IconProps) => {
    return <Shopee width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  LAZADA: ({size = 20, fill='none'}: IconProps) => {
    return <Lazada width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  TIKI: ({size = 20, fill='none'}: IconProps) => {
    return <Tiki width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  LOCATION_MAP: ({size = 36, fill='none'}: IconProps) => {
    return <LocationMap width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  REQUEST_LOCATION: ({size = 74, fill='none'}: IconProps) => {
    return <RequestLocation width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  GOLD: ({width = 40, height = 15, fill='none'}: IconProps) => {
    return <Gold width={scaleSizeWidth(width)} height={scaleSizeWidth(height)} fill={fill} />;
  },
  DIRECTION: ({size = 32, fill='none'}: IconProps) => {
    return <Direction width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  SCAN_RED: ({size = 48, fill='none'}: IconProps) => {
    return <ScanRed width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
  FILTER_OUTLINE: ({size = 16, fill='none'}: IconProps) => {
    return <FilterOutline width={scaleSizeWidth(size)} height={scaleSizeWidth(size)} fill={fill} />;
  },
};