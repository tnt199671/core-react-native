import axios from 'axios';
import store from '@store';

const client = axios.create({
  baseURL: 'https://abc.com',
  timeout: 30000,
  headers: {
    "Content-Type": "application/json",
    Accept: "*/*",
  },
});

client.interceptors.request.use(
  async config => {
    const {token} = store.getState()?.auth;
    const cloneConfig = {...config};
    if (token) {
      cloneConfig.headers.authorization = `Bearer ${token}`;
    }
    // // language
    cloneConfig.headers.locale = 'au';

    return cloneConfig;
  },
  error => Promise.reject(error),
);

// Add a response interceptor
client.interceptors.response.use(
  response => {
    return response;
  },
  async error => {
    console.log('API ERROR:', error?.message, error);
    if (error?.response?.status === 401) {
      return Promise.resolve(error);
    }
    if (error?.response?.status === 500) {
      return Promise.resolve(error.response);
    }
    if (!error?.response) {
      // timeout
      return Promise.resolve(error);
    }
    return Promise.resolve(error?.response || error);
  },
);

export default client;
