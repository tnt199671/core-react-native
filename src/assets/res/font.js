export default {
  'w_300': 'OpenSans-Light',
  'w_400': 'OpenSans-Regular',
  'w_500': 'OpenSans-Medium',
  'w_600': 'OpenSans-SemiBold',
  'w_700': 'OpenSans-Bold',
  'w_800': 'OpenSans-ExtraBold',
};