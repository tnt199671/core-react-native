import {
  UPDATE_AUTH_STORE, 
} from '../actions/auth';

const initState = {
  token: undefined,
  user: undefined,
  fcmToken: undefined,
};

const auth = (state = initState, action: any) => {
  switch (action.type) {
    case UPDATE_AUTH_STORE:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};

export default auth;
