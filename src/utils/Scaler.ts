import {Dimensions, Platform, NativeModules} from 'react-native';
import Device from 'react-native-device-info';
const {StatusBarManager} = NativeModules;

const guidelineBaseWidth = 414;
const guidelineBaseHeight = 896;

export const WINDOW_WIDTH = Dimensions.get('window').width;
export const WINDOW_HEIGHT = Dimensions.get('window').height;
export const StatusBarHeight = StatusBarManager.HEIGHT;

export const scaleSizeWidth = (size: number) => {
  return (WINDOW_WIDTH / guidelineBaseWidth) * size;
}

export const scaleSizeHeight = (size: number) => {
  return (WINDOW_HEIGHT / guidelineBaseHeight) * size;
}

export const isIphoneX = () => {
  return (
    Platform.OS === 'ios' &&
    !Platform.isPad &&
    (Device.hasNotch() || Device.hasDynamicIsland())
  );
};
export const getBottomSpace = () => {
  return isIphoneX() ? 34 : 0;
};
