import {combineReducers} from 'redux';
import auth from './auth';
import global from './global';

const rootReducer = combineReducers({
  auth,
  global
});

export default rootReducer;
