import * as Yup from 'yup';
import store from '@store';
const { lang } = store.getState().global as any;

export const FORM_TEST_VALIDATION = Yup.object().shape({
  user: Yup.string().nullable()
    .required(lang.form.required)
    .max(10, lang.form.phone_max),
  password: Yup.string().nullable()
    .required(lang.form.required)
    .max(32, lang.form.password_max)
    .min(8, lang.form.password_min),
  image: Yup.string().nullable(),
});

export const LOGIN_VALIDATION = Yup.object().shape({
  user: Yup.string().nullable()
    .required(lang.form.required)
    .max(10, lang.form.phone_max),
  password: Yup.string().nullable()
    .required(lang.form.required)
    .max(32, lang.form.password_max)
    .min(8, lang.form.password_min),
});