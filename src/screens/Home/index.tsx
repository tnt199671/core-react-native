import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Colors } from '@assets';
import { AppHeader, DismissKeyboard } from '@components';
import { scaleSizeWidth } from '@utils/Scaler';
import BlockLoading from './components/BlockLoading';
import BlockToast from './components/BlockToast';
import BlockForm from './components/BlockForm';
import BlockInputSearch from './components/BlockInputSearch';
import BlockPopup from './components/BlockPopup';
import BlockSheet from './components/BlockSheet';

const Home = () => {
  return (
    <View style={styles.container}>
      <AppHeader title={'Home'} />
      <ScrollView
        style={styles.scrollView} 
        contentContainerStyle={styles.contentContainerStyle}
        showsVerticalScrollIndicator={false}
      >
        <BlockLoading />
        <BlockToast />
        <BlockPopup />
        <BlockSheet />
        <BlockInputSearch />
        <BlockForm />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    height: '100%',
  },
  scrollView: {
    flex: 1,
    padding: scaleSizeWidth(20),
  },
  contentContainerStyle: {
    paddingBottom: scaleSizeWidth(100),
  }
});

export default React.memo(Home);
