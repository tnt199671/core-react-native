import React, { useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { AppButton } from '@components';
import { scaleSizeWidth } from '@utils/Scaler';
import { POPUP_TYPE } from '@constants';
import { showPopup } from '@components/Popup';

const BlockPopup = () => {

  const onPress = useCallback(() => {
    showPopup(POPUP_TYPE.DEFAULT, () => null, {
      title: 'hihihi',
      content: 'test ne :D'
    });
  }, []);

  return (
    <AppButton
      title={'Test Popup'}
      onPress={onPress}
      style={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: scaleSizeWidth(10),
  }
});

export default React.memo(BlockPopup);
