export const SHOW_LOADING = 'SHOW_LOADING';
export const UPDATE_GLOBAL_STORE = 'UPDATE_GLOBAL_STORE';
export const APP_MOUNTED = 'APP_MOUNTED';
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';

export const showLoadingAction = (visible: boolean) => ({
  type: SHOW_LOADING,
  visible,
});

export const updateGlobalStoreAction = (payload: UpdateGlobalStoreActionParams) => ({
  type: UPDATE_GLOBAL_STORE,
  payload,
});

export const appMountedAction = () => ({
  type: APP_MOUNTED
});

export const changeLanguageAction = (language: string) => ({
  type: CHANGE_LANGUAGE,
  language,
});