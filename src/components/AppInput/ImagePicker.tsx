import React, {useCallback} from 'react';
import {View, StyleSheet, TouchableOpacity, Platform, Image, TextStyle } from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Colors, Fonts, Icons} from '@assets';
import {scaleSizeWidth, scaleSizeHeight} from '@utils/Scaler';
import {checkAndRequestCameraPermissions, checkPermisionPhotoLibrary} from '@utils/HardwarePermission';
import {compressorImage} from '@utils';
import {PERMISSIONS, RESULTS} from 'react-native-permissions';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {MEDIA_TYPE} from '@constants';
import {AppText} from '@components';
import { RootState } from '@store';

type ImagePickerProps = {
  onChange: (value: any) => void;
  value: string;
  label: string;
  mediaType?: string;
  styleLabel?: TextStyle;
};

const ImagePicker = ({onChange, value, label, mediaType, styleLabel}: ImagePickerProps) => {
  const dispatch = useDispatch();
  const { lang } = useSelector((state:RootState) => state.global) as any;

  const onOpenCamera = useCallback(async () => {
    const grantedResults = await checkAndRequestCameraPermissions('photo');
    let granted = true;
    if(Platform.OS === 'android' && grantedResults[PERMISSIONS.ANDROID.CAMERA] !== RESULTS.GRANTED){
      granted = false;
    }
    if(Platform.OS === 'ios' && grantedResults[PERMISSIONS.IOS.CAMERA] !== RESULTS.GRANTED){
      granted = false;
    }
    if(granted){
      launchCamera({mediaType: 'photo', presentationStyle: 'fullScreen'}, async (response) => {
        if(!response?.didCancel) {
          const images = await compressorImage(response.assets); // down size image
          onChange(images?.[0])
        }
      });
    }
  }, [onChange]);

  const onOpenLibrary = useCallback(async () => {
    let grantedResults = true;
    if (Platform.OS === 'ios') {
      grantedResults = await checkPermisionPhotoLibrary();
    }
    if (grantedResults) {
      const result = await launchImageLibrary({
        mediaType: 'photo', 
        presentationStyle: 'fullScreen',
        selectionLimit: 1,
      });
      if(!result?.didCancel){
        const images = await compressorImage(result.assets);
        setTimeout(() => {
          onChange(images?.[0]);
        }, 300);
      }
    }
  }, [onChange]);

  const onRemove = useCallback(() => {
    onChange(null);
  }, [onChange])

  return (
    <View style={styles.container}>
      <AppText size={13} lineHeight={24} fontFamily={Fonts.w_700} color={Colors.text01} mb={16} style={styleLabel}>
        {label}
      </AppText>
      {value ?
        <View>
          <Image 
            source={{uri: `${value}`}}
            style={styles.image}
          />
          <TouchableOpacity style={styles.close} onPress={onRemove}>
            <Icons.CLOSE/>
          </TouchableOpacity>
        </View>
      :
        <View style={styles.wrapper}>
          {(mediaType === MEDIA_TYPE.CAMERA || mediaType === MEDIA_TYPE.MIXED) &&
            <TouchableOpacity style={styles.itemContainer} onPress={onOpenCamera}>
              <Icons.CAMERA/>
              <AppText size={16} lineHeight={23} color={Colors.text01} mt={11} textAlign={'center'}>
                {lang.form.new_shoot}
              </AppText>
            </TouchableOpacity>
          }
          {(mediaType === MEDIA_TYPE.LIBRARY || mediaType === MEDIA_TYPE.MIXED) &&
            <TouchableOpacity style={styles.itemContainer} onPress={onOpenLibrary}>
              <Icons.LIBRARY/>
              <AppText size={16} lineHeight={23} color={Colors.text01} mt={11} textAlign={'center'}>
                {lang.form.select_library}
              </AppText>
            </TouchableOpacity>
          }
        </View>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    paddingBottom: scaleSizeHeight(20)
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    gap: scaleSizeWidth(10),
  },
  itemContainer: {
    flex: 1,
    backgroundColor: Colors.background02,
    borderRadius: scaleSizeWidth(10),
    alignItems: 'center',
    paddingVertical: scaleSizeHeight(20),
  },
  image: {
    height: scaleSizeHeight(177),
    borderRadius: scaleSizeWidth(15)
  },
  close: {
    position: 'absolute',
    right: 0
  }
});

export default React.memo(ImagePicker);
