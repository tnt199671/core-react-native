import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import sagaPlugin from 'reactotron-redux-saga';

const dev = __DEV__;

function configure() {
  Reactotron
    .configure({host: '192.168.1.158'}) // ip địa chỉ mạng (Mac: ifconfig)
    // .configure({host: '169.254.54.157'})
    .configure() // controls connection & communication settings
    .useReactNative() // add all built-in react native plugins
    .use(sagaPlugin())
    .use(reactotronRedux())


  connectConsoleToReactotron()
  return Reactotron.connect()
}


function connectConsoleToReactotron() {
  console.log = Reactotron.log;
  if (!dev) return
  //  console.log = Reactotron.log;
  // console.warn = Reactotron.warn;
  // console.error = Reactotron.error;
}

export default {
  configure,
}