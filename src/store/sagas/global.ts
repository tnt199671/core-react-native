import {Platform} from 'react-native';
import {put, call, takeLatest, select, all} from 'redux-saga/effects';
import {
  APP_MOUNTED,
  showLoadingAction,
  updateGlobalStoreAction
} from '@store/actions/global';
import NavigationService from '@services/navigation';
import SplashApp from 'react-native-splash-screen';
import { ScreenNames } from '@navigator/ScreenNames';
import { RootState } from '@store';

export function* appMounted(): any {
  try {
    yield put(showLoadingAction(true));
    const user = yield select((state: RootState) => state.auth?.user);
    if(user){
      NavigationService.reset(ScreenNames.BottomTabBar);
    }else{
      // NavigationService.reset(ScreenNames.OnboardingScreen);
      NavigationService.reset(ScreenNames.BottomTabBar);
    }
  } catch (e) {
    console.warn(e)
  } finally {
    SplashApp.hide();
    yield put(showLoadingAction(false));
  }
}

function* GlobalSaga(): any {
  yield takeLatest(APP_MOUNTED, appMounted);
}

export default GlobalSaga;