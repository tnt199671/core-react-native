import {
  check,
  checkMultiple,
  requestMultiple,
  request,
  openSettings,
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions';
import {Alert, Platform} from 'react-native';

export const checkIosCameraPermission = async (onCancel, onResult) => {
  const result = await check(PERMISSIONS.IOS.CAMERA);
  if (result === RESULTS.GRANTED) {
    onResult(true);
  } else if (result === RESULTS.DENIED || result === RESULTS.LIMITED) {
    request(PERMISSIONS.IOS.CAMERA).then(requestResult => {
      if (requestResult !== RESULTS.GRANTED) {
        onCancel();
      } else {
        onResult(requestResult === RESULTS.GRANTED);
      }
    });
  } else if (result === RESULTS.BLOCKED) {
    Alert.alert(
      '',
      'Please enable camera access in app settings.',
      [
        {
          text: 'Cancel',
          onPress: () => {
            onCancel();
          },
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            openSettings().catch(() => console.warn('cannot open settings'));
          },
        },
      ],
    );
    return false;
  }
};

export const checkAndroidCameraPermission = async (onCancel, onResult) => {
  const result = await check(PERMISSIONS.ANDROID.CAMERA);
  if (result === RESULTS.GRANTED) {
    onResult(true);
  } else {
    request(PERMISSIONS.ANDROID.CAMERA).then(requestResult => {
      if (requestResult === RESULTS.GRANTED) {
        onResult(true);
      } else {
        Alert.alert(
          '',
          'Please enable camera access in app settings.',
          [
            {
              text: 'Cancel',
              onPress: () => {
                onCancel();
              },
              style: 'cancel',
            },
            {
              text: 'OK',
              onPress: () => {
                openSettings().catch(() =>
                  console.warn('cannot open settings'),
                );
              },
            },
          ],
        );
        return false;
      }
    });
  }
};

export const checkCameraPermission = async ({onCancel, onResult}) => {
  if (Platform.OS === 'ios') {
    checkIosCameraPermission(onCancel, onResult);
  } else {
    checkAndroidCameraPermission(onCancel, onResult);
  }
};

export const checkAndRequestIOSCameraPermission = async mediaType => {
  let permissionsToCheck = [];
  if (mediaType === 'video') {
    permissionsToCheck = [PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.MICROPHONE];
  } else if (mediaType === 'photo') {
    permissionsToCheck = [PERMISSIONS.IOS.CAMERA];
  }

  const checkResults = await checkMultiple(permissionsToCheck);

  console.log('check results');
  console.log(checkResults);

  const checkResultsValues = Object.values(checkResults);
  const allGranted = !checkResultsValues?.find(
    item => item !== RESULTS.GRANTED,
  );

  if (allGranted) {
    return checkResults;
  }
  const cannotProcess = checkResultsValues?.find(
    item => ![RESULTS.GRANTED, RESULTS.DENIED].includes(item),
  );

  if (cannotProcess) {
    return checkResults;
  }

  // request permissions
  const permissionsToRequest = [];
  if (checkResults[PERMISSIONS.IOS.CAMERA] === RESULTS.DENIED) {
    permissionsToRequest.push(PERMISSIONS.IOS.CAMERA);
  }

  if (checkResults[PERMISSIONS.IOS.MICROPHONE] === RESULTS.DENIED) {
    permissionsToRequest.push(PERMISSIONS.IOS.MICROPHONE);
  }

  const requestResults = await requestMultiple(permissionsToRequest);

  console.log('request results');
  console.log(requestResults);

  return requestResults;
};

export const checkAndRequestAndroidCameraPermissions = async mediaType => {
  let permissionsToCheck = [];
  if (mediaType === 'video') {
    permissionsToCheck = [
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.RECORD_AUDIO,
    ];
  } else if (mediaType === 'photo') {
    permissionsToCheck = [PERMISSIONS.ANDROID.CAMERA];
  }

  const checkResults = await checkMultiple(permissionsToCheck);

  console.log('check results');
  console.log(checkResults);

  const checkResultsValues = Object.values(checkResults);
  const allGranted = !checkResultsValues?.find(
    item => item !== RESULTS.GRANTED,
  );
  console.log('allGranted:', allGranted);
  if (allGranted) {
    return checkResults;
  }
  const cannotProcess = checkResultsValues?.find(
    item => ![RESULTS.GRANTED, RESULTS.DENIED].includes(item),
  );

  if (cannotProcess) {
    return checkResults;
  }

  // request permissions
  const permissionsToRequest = [];
  if (checkResults[PERMISSIONS.ANDROID.CAMERA] === RESULTS.DENIED) {
    permissionsToRequest.push(PERMISSIONS.ANDROID.CAMERA);
  }

  if (checkResults[PERMISSIONS.ANDROID.RECORD_AUDIO] === RESULTS.DENIED) {
    permissionsToRequest.push(PERMISSIONS.ANDROID.RECORD_AUDIO);
  }

  const requestResults = await requestMultiple(permissionsToRequest);

  console.log('request results');
  console.log(requestResults);

  return requestResults;
};

// mediaType = video | photo
export const checkAndRequestCameraPermissions = async mediaType => {
  if (Platform.OS === 'android') {
    return await checkAndRequestAndroidCameraPermissions(mediaType);
  } else if (Platform.OS === 'ios') {
    return await checkAndRequestIOSCameraPermission(mediaType);
  } else {
    throw new Error('This platform is not supported');
  }
};

export const checkPermissionWriteExternalStorage = async () => {
  try {
    const result = await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
    if (result === RESULTS.GRANTED) {
      return true;
    }
    Alert.alert(
      '',
      'Please enable camera access in app settings.',
      [
        {
          text: 'OK',
          onPress: () => {
            openSettings().catch(() => console.warn(`Can't open settings`));
          },
        },
      ],
      {cancelable: false},
    );
  } catch (err) {
    Alert.alert(
      'Save',
      'Save Error: ' + err.message,
      [{text: 'OK', onPress: () => console.log('OK Pressed')}],
      {cancelable: false},
    );
  }
};


export const checkPermisionPhotoLibrary = async () => {
  const result = await check(PERMISSIONS.IOS.PHOTO_LIBRARY);
  if (result === RESULTS.GRANTED) {
    return true;
  } else {
    request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(requestResult => {
      if (requestResult === RESULTS.GRANTED) {
        return true;
      } else {
        Alert.alert(
          `You don't have permission to access the photo gallery`,
          'Please grant gallery access to share photos directly to the app.',
          [
            {
              text: 'Cancel',
              style: 'cancel',
            },
            {
              text: 'Settings',
              onPress: () => {
                openSettings().catch(() =>
                  console.warn('cannot open settings'),
                );
              },
            },
          ],
        );
        return false;
      }
    });
    return false;
  }
};

export const checkPermisionLocation = async () => {
  const permission = Platform.OS === 'android' ?  PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
  return await check(permission)
    .then((result) => {
      switch (result) {
        case RESULTS.UNAVAILABLE:
          // console.log('This feature is not available (on this device / in this context)');
          return false;
        case RESULTS.DENIED:
          // console.log('The permission has not been requested / is denied but requestable');
          return false;
        case RESULTS.LIMITED:
          // console.log('The permission is limited: some actions are possible');
          return true;
        case RESULTS.GRANTED:
          // console.log('The permission is granted');
          return true;
        case RESULTS.BLOCKED:
          // console.log('The permission is denied and not requestable anymore');
          return false;
    }
  })
  .catch((error) => {
    // …
  });
};

export const requestPermissionLocation = async () => {
  return await request(
    Platform.select({
      android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      ios: PERMISSIONS.IOS.LOCATION_WHEN_IN_USE
    })
  ).then(res => {
    if (res == RESULTS.GRANTED) {
      return true;
    } else {
      return false;
    }
  });
}
