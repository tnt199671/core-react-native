import React, {useRef, memo, useState, useCallback, useMemo} from 'react';
import {View} from 'react-native';
import ActionSheet, {
  registerSheet,
  SheetProps,
  SheetManager
} from 'react-native-actions-sheet';
import {SHEET_TYPE, SHEET} from '@constants';
import DatePicker from './DatePicker';

type DataProps = {
  type: SHEET_TYPE;
  data?: any;
  callback: (value: any, value2?: any) => void | undefined;
};

function Sheet({sheetId}: SheetProps) {
  const actionSheetRef = useRef(null);
  const [state, setState] = useState({
    type: SHEET_TYPE.DEFAULT,
    data: null,
    callback: (value: any, value2?: any) => {}
  });

  const onBeforeShow = useCallback((data: DataProps) => {
    setState({
      ...state,
      ...data
    })
  }, [state]);

  const onClose = useCallback(() => {
    setState({
      type: SHEET_TYPE.DEFAULT,
      data: null,
      callback: () => {}
    })
  }, []);

  const onHide = useCallback(() => {
    SheetManager.hide(SHEET.DEFAULT);
  }, []);

  const renderContent = useMemo(() => {
    switch(state.type) {
      case SHEET_TYPE.DATE_PICKER:
        return <DatePicker {...state} onHide={onHide}/>;
      default:
        return <View/>
    }
  }, [state]);

  return (
    <ActionSheet
      onBeforeShow={onBeforeShow as (data?: any) => void}
      id={sheetId}
      ref={actionSheetRef}
      drawUnderStatusBar={true}
      gestureEnabled={true}
      defaultOverlayOpacity={0.7}
      closeOnTouchBackdrop={true}
      headerAlwaysVisible={true}
      onClose={onClose}
      >
        {renderContent}
    </ActionSheet>
  );
}

export default memo(Sheet);

registerSheet(SHEET.DEFAULT, Sheet);