export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const UPDATE_AUTH_STORE = 'UPDATE_AUTH_STORE';

export const loginAction = (params: LoginActionParams) => ({
  type: LOGIN,
  params
});

export const logoutAction = () => ({
  type: LOGOUT
});

export const updateAuthStoreAction = (payload: UpdateAuthStoreActionParams) => ({
  type: UPDATE_AUTH_STORE,
  payload,
});

