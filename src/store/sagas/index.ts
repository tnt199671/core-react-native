import {fork} from '@redux-saga/core/effects';
import AuthSaga from './auth';
import GlobalSaga from './global';

export default function* rootSaga() {
  yield fork(AuthSaga);
  yield fork(GlobalSaga);
}
