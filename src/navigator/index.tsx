import React, {useEffect, useState} from 'react';
import {StatusBar, AppState, Platform} from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ScreenNames} from './ScreenNames';
import {navigationRef} from '@services/navigation';
import {useDispatch, useSelector} from 'react-redux';
import {appMountedAction, updateGlobalStoreAction} from '@store/actions/global';
import Orientation, {
  useDeviceOrientationChange,
} from 'react-native-orientation-locker';
import {ORIENTATION} from '@constants';
import {isTablet} from '@utils/Scaler';
import notifee from '@notifee/react-native';

import BottomTabNavigator from './bottomTab';
import {
  Login,
} from '@screens';

const Navigator = React.memo(() => {
  const dispatch = useDispatch();
  const orientation = useSelector(state => state.global.orientation);
  const Stack = createNativeStackNavigator();

  useEffect(() => {
    const subscription = AppState.addEventListener('change', nextAppState => {
      dispatch(updateGlobalStoreAction({
        appState: nextAppState
      }));
    });
    !isTablet && Orientation.lockToPortrait(); 

    return () => {
      subscription.remove();
    };
  }, []);

  const onReady = async () => {
    setTimeout(() => {
      dispatch(appMountedAction());
    }, 1000);
  };

  useDeviceOrientationChange((o) => {
    let newOrientation = o;
    if(o === ORIENTATION.PORTRAIT_UPSIDEDOWN || o === ORIENTATION.FACE_UP){
      newOrientation = orientation;
    }
    dispatch(updateGlobalStoreAction({
      orientation: newOrientation
    }));
  });

  return (
    <SafeAreaProvider>
      <NavigationContainer theme={DefaultTheme} ref={navigationRef} onReady={onReady}>
        <Stack.Navigator
          initialRouteName={ScreenNames.Login}
          screenOptions={{
            headerShown: false,
            animation: 'slide_from_right',
            presentation: 'card'
          }}
        >
          <Stack.Screen name={ScreenNames.Login} component={Login}/>
          <Stack.Screen name={ScreenNames.BottomTabBar} component={BottomTabNavigator} />
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaProvider>
  );
});

export default Navigator;
