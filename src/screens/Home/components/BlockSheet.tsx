import React, { useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { AppButton } from '@components';
import { scaleSizeWidth } from '@utils/Scaler';
import { SHEET, SHEET_TYPE } from '@constants';
import { SheetManager } from 'react-native-actions-sheet';

const BlockSheet = () => {

  const onPress = useCallback(() => {
    SheetManager.show(SHEET.DEFAULT, {
        payload: {
          type: SHEET_TYPE.DATE_PICKER,
          data: {value: new Date(), title: 'test ne hihi'},
          callback: () => null
        } as any
      })
  }, []);

  return (
    <AppButton
      title={'Test Sheet'}
      onPress={onPress}
      style={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: scaleSizeWidth(10),
  }
});

export default React.memo(BlockSheet);
