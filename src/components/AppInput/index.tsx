import React, { useCallback, forwardRef } from 'react';
import { TextInput, View, ViewStyle } from 'react-native';
import { Controller } from "react-hook-form";
import { INPUT_TYPE, SHEET, SHEET_TYPE } from '@constants';
import { SheetManager } from 'react-native-actions-sheet';
import Input from './Input';
import ImagePicker from './ImagePicker';

const arrayDefaultInputs = [
  INPUT_TYPE.DEFAULT,
  INPUT_TYPE.DATE_PICKER,
];

type AppInputProps = {
  control?: any;
  field: string;
  type?: string;
  label: string;
  stylesContainer?: ViewStyle;
  watch?: any;
  setValue?: any;
  title?: string;
  error?: string;
};

const AppInput = ({
  control,
  field,
  type = INPUT_TYPE.DEFAULT,
  label,
  stylesContainer,
  watch,
  setValue,
  title,
  ...props
}: AppInputProps, ref: React.LegacyRef<TextInput> | undefined) => {

  const onPress = useCallback((onChange: (value: any) => void, value: string) => {
    if (type === INPUT_TYPE.DATE_PICKER) {
      SheetManager.show(SHEET.DEFAULT, {
        payload: {
          type: SHEET_TYPE.DATE_PICKER,
          data: { value, title: title },
          callback: onChange
        } as any
      })
    }
  }, [type, title, watch, setValue]);

  return (
    <View style={stylesContainer}>
      <Controller
        name={field}
        control={control}
        render={({ field: { onChange, value } }) => (
          <>
            {arrayDefaultInputs.includes(type as INPUT_TYPE) && (
              <Input
                ref={ref}
                {...props}
                value={value}
                type={type as INPUT_TYPE}
                label={label}
                onChange={onChange}
                onPress={onPress}
              />
            )}
            {type === INPUT_TYPE.IMAGE_PICKER && (
              <ImagePicker
                {...props}
                value={value}
                label={label}
                onChange={onChange}
              />
            )}
          </>
        )}
      />
    </View>
  );
};

export default forwardRef(AppInput);
