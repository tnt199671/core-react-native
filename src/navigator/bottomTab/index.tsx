import React from 'react';
import {View} from 'react-native';
import {BottomTabNavigationOptions, createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {ScreenNames} from '../ScreenNames';
import BottomTabBar from './BottomTabBar';
import {
  Home, 
  Scan,
} from '@screens';
import {Icons} from '@assets';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {

  return (
    <Tab.Navigator
      sceneContainerStyle={{backgroundColor: 'transparent'}}
      screenOptions={{
        headerShown: false
      }}
      tabBar={props => <BottomTabBar {...props} />}
    >
      <Tab.Screen
        name={ScreenNames.Home}
        component={Home}
        options={{
          title: 'Home',
          icon: Icons.MAIN_HOME
        } as BottomTabNavigationOptions}
      />
      <Tab.Screen
        name={ScreenNames.Gift}
        component={View}
        options={{
          title: 'Claim',
          icon: Icons.MAIN_GIFT
        } as BottomTabNavigationOptions}
      />
      <Tab.Screen
        name={ScreenNames.Scan}
        component={Scan}
        options={{
          title: 'Scan',
          icon: Icons.MAIN_SCAN
        } as BottomTabNavigationOptions}
      />
      <Tab.Screen
        name={ScreenNames.Tip}
        component={View}
        options={{
          title: 'Tips',
          icon: Icons.MAIN_TIP
        } as BottomTabNavigationOptions}
      />
      <Tab.Screen
        name={ScreenNames.Shop}
        component={View}
        options={{
          title: 'Shop',
          icon: Icons.MAIN_CART
        } as BottomTabNavigationOptions}
      />
    </Tab.Navigator>
  );
};

export default React.memo(BottomTabNavigator);
