export default {
  white: 'white',
  black: 'black',
  main: '#E4051D',
  transparent: 'rgba(0, 0, 0, 0.0)',


  background01: '#FFE5E8',
  background02: '#EFEFEF',
  background03: 'rgba(0,0,0,0.4)',
  background04: '#62A816',
  background05: '#DF4F5F',
  background06: '#F59B0A',
  background07: 'rgba(0, 0, 0, 0.05)',

  border01: 'rgba(0, 0, 0, 0.10)',
  border02: '#E4051D',
  
  button01: '#1414A0',

  text01: 'rgba(0, 0, 0, 0.50)',
  text02: 'rgba(0, 0, 0, 0.30)',
  text03: '#155724',
  text04: '#721C24',
  text05: '#856404',
};