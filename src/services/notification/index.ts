import React, {useEffect, useState} from 'react';
import { Platform, PermissionsAndroid } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import notifee from '@notifee/react-native';

const Notification = (props) => {
  const dispatch = useDispatch();

  const [channelId, setChannelId] = useState('default');

  const init = async () => {
    await notifee.requestPermission();
    const id = await notifee.createChannel({
      id: 'thuan',
      name: 'thuan Channel',
    });
    setChannelId(id);
    console.log(await messaging().getToken());
    
    // const a = await notifee.cancelNotification('0', '14cw');
  }

  const pushNotiLocal = async (message) => {
    await notifee.displayNotification({
      title: message?.notification?.title,
      body: message?.notification?.body,
      android: {
        channelId,
      },
    });
  }

  useEffect(() => {
    init();
  }, []);
  
  useEffect(() => {
    // Usesd to display notification when app is in foreground
    const unsubscribe = messaging().onMessage(async remoteMessage => {
      console.log('remoteMessage::', remoteMessage);
      pushNotiLocal(remoteMessage, true);
    });

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage,
      );
      pushNotiLocal(remoteMessage);
    });

    messaging().getInitialNotification().then(remoteMessage => {
      if(remoteMessage){
        console.log(111111, remoteMessage); // always prints null
        pushNotiLocal(remoteMessage);
      }
    });

    return unsubscribe;
  }, []);

  return null;
};

export default React.memo(Notification);
