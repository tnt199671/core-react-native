import React, {memo, useRef, useEffect, useState, useCallback, useMemo} from "react";
import {
  Modal,
  View,
  StyleSheet
} from "react-native";
import { Colors } from "@assets";
import {useDispatch} from 'react-redux';
import {POPUP_TYPE} from '@constants';
import Default from './Default';

interface Ref {
  show: (type: POPUP_TYPE, data: any, callback: (value: any) => void) => void;
  hide: () => void;
}

let globalRef: React.MutableRefObject<Ref | undefined> = useRef();

export const showPopup = (type: POPUP_TYPE, callback: (value: any) => void, data?: any): void => {
  globalRef.current?.show(type, data, callback);
};

export const hidePopup = () => {
  globalRef.current?.hide();
};

const Popup = () => {
  const ref = useRef<Ref>();
  const dispatch = useDispatch();
  const [state, setState] = useState({
    visible: false,
    type: POPUP_TYPE.DEFAULT,
    data: null,
    callback: (value: any) => {}
  });

  useEffect(() => {
    globalRef = ref;
  }, []);

  React.useImperativeHandle(ref, () => ({
    show,
    hide
  }));

  const show = useCallback((type: POPUP_TYPE, data: any, callback: (value: any) => void) => {
    setState({
      visible: true,
      type,
      data,
      callback
    });
  }, []);

  const hide = useCallback(() => {
    setState({
      visible: false,
      type: POPUP_TYPE.DEFAULT,
      callback: () => {},
      data: null
    });
  }, []);

  const renderContent = useMemo(() => {
    switch(state.type) {
      case POPUP_TYPE.DEFAULT:
        return <Default onClose={hide} {...state}/>;
      default:
        return <View/>
    }
  }, [state]);

  return (
    <Modal 
      transparent 
      visible={state.visible} 
      statusBarTranslucent
      supportedOrientations={['portrait', 'landscape']}
    >
      <View style={styles.container}>
        {renderContent}
      </View>
    </Modal>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background07,
    justifyContent: "center",
  },
});

export default memo(Popup);
