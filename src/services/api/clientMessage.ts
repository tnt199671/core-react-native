import axios from 'axios';
import store from '@store';

const client = axios.create({
  baseURL: 'https://fcm.googleapis.com/fcm',
  timeout: 30000,
  headers: {
    "Content-Type": "application/json",
    "Accept": "*/*",
    "Authorization": "key=AAAAXFIUEAU:APA91bHAEjB4EeA51A2XSFcwWJw1y9I1Yulaj9WbaIA_QKJot2eQm1EkftAFVEHm8n9Q5-oLg2Oc0B86oAZtY3dTndF7VhYVVkEqiisEc1jCN79zpbZ0MSFhWDZHKvWnxUSixCntoBWz"
  },
});

client.interceptors.request.use(
  async config => {
    const {token} = store.getState()?.auth;
    const cloneConfig = {...config};
    // if (token) {
    //   cloneConfig.headers.authorization = `Bearer ${token}`;
    // }
    // // language
    cloneConfig.headers.locale = 'au';

    return cloneConfig;
  },
  error => Promise.reject(error),
);

// Add a response interceptor
client.interceptors.response.use(
  response => {
    return response;
  },
  async error => {
    console.log('API ERROR:', error?.message, error);
    if (error?.response?.status === 401) {
      return Promise.resolve(error);
    }
    if (error?.response?.status === 500) {
      return Promise.resolve(error.response);
    }
    if (!error?.response) {
      // timeout
      return Promise.resolve(error);
    }
    return Promise.resolve(error?.response || error);
  },
);

export default client;
