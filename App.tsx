import React, { useRef, useEffect } from 'react';
import { StatusBar, View, Platform} from 'react-native';
import Navigator from './src/navigator';
import {Provider} from 'react-redux';
import store from '@store';
import {SheetProvider} from 'react-native-actions-sheet';
import {Loading, Toast, Popup} from '@components';
import '@components/Sheet';
import Notification from '@services/notification';

const App = () => {
  return (
    <Provider store={store}>
      <SheetProvider>
        <StatusBar 
          barStyle={"dark-content"} 
          translucent={true} 
          backgroundColor={'transparent'} 
        />
        <Navigator/>
        <Popup/>
        <Loading/>
        <Toast/>
        <Notification/>
      </SheetProvider>
    </Provider>
  );
}

export default App;
