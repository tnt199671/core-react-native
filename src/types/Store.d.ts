type UpdateGlobalStoreActionParams = {
    isLoading?: boolean;
    appState?: any;
    orientation?: string;
    lang?: any;
};

type LoginActionParams = {
    user: string;
    password: string;
};

type UpdateAuthStoreActionParams = {
    token?: string;
    user?: any;
    fcmToken?: string;
};