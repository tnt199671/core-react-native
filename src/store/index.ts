import {applyMiddleware, compose, createStore} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {persistStore, persistReducer} from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducers';
import rootSaga from './sagas';
import Reactotron from 'reactotron-react-native';
import ReactotronConfig from '@reactotronConfig';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['auth'],
  timeout: 0,
};

const composeEnhancers =
  (typeof window !== 'undefined' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

//Add middleware
const reactotron = ReactotronConfig.configure();
const sagaMonitor = Reactotron.createSagaMonitor();
const sagaMiddleware = createSagaMiddleware({ sagaMonitor });
const enhancer = composeEnhancers(applyMiddleware(sagaMiddleware));
const Store = createStore(persistReducer(persistConfig, rootReducer), enhancer);
Reactotron.clear();
sagaMiddleware.run(rootSaga);

export const persistor = persistStore(Store);
export type RootState = ReturnType<typeof Store.getState>;

export default Store;
