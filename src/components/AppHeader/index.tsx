import React, {ReactNode, useCallback} from 'react';
import {View, Image, TouchableOpacity, StyleSheet, ViewStyle} from 'react-native';
import NavigationService from '@services/navigation';
import {Colors, Icons, Fonts, Images} from '@assets';
import {StatusBarHeight, scaleSizeWidth} from '@utils/Scaler';
import {DismissKeyboard, AppText} from '@components';

type Props = {
  onBack?: () => void;
  style?: ViewStyle;
  title?: string;
  isLogo?: boolean;
  textColor?: string;
  isDarkMode?: boolean;
  renderContent?: ReactNode;
  hideLeft?: boolean;
};

const AppHeader = ({
  onBack, 
  style, 
  title, 
  isLogo,
  textColor = Colors.black,
  isDarkMode = false,
  renderContent,
  hideLeft = false
}: Props) => {
  const onPress = useCallback(() => {
    NavigationService.goBack();
  }, []);

  return (
    <DismissKeyboard>
      <View style={[
        styles.container, 
        isDarkMode && {backgroundColor: Colors.main},
        style
      ]}>
        {!hideLeft &&
          <TouchableOpacity onPress={onBack || onPress}>
            {isDarkMode ? <Icons.BACK_WHITE/> : <Icons.BACK/>}
          </TouchableOpacity>
        }
        {renderContent ? renderContent :
          <View style={[styles.content, hideLeft && {marginRight: 0}]}>
            {isLogo &&
              <Image
                source={Images.Logo}
                style={styles.logo}
              />
            }
            {title && <AppText size={17} lineHeight={35} color={textColor} fontFamily={Fonts.w_600}>{title}</AppText>}
          </View>
        }
      </View>
    </DismissKeyboard>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: StatusBarHeight,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scaleSizeWidth(18),
    backgroundColor: Colors.white
  },
  content: {
    flex: 1,
    marginRight: scaleSizeWidth(30),
    alignItems: 'center',
    justifyContent: 'center'
  },
  logo: {
    width: scaleSizeWidth(176),
    height: scaleSizeWidth(64)
  }
});

export default React.memo(AppHeader);
