import React, { useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { AppButton } from '@components';
import { showLoadingAction } from '@store/actions/global';
import { useDispatch } from 'react-redux';
import { scaleSizeWidth } from '@utils/Scaler';

const BlockLoading = () => {
  const dispatch = useDispatch();

  const testLoading = useCallback(() => {
    dispatch(showLoadingAction(true));
    setTimeout(() => {
      dispatch(showLoadingAction(false));
    }, 2000);
  }, []);

  return (
    <AppButton
      title={'Test Loading'}
      onPress={testLoading}
      style={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: scaleSizeWidth(10),
  }
});

export default React.memo(BlockLoading);
