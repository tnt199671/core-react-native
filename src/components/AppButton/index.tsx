import React from 'react';
import {Text, TouchableHighlight, StyleSheet, ViewStyle} from 'react-native';
import {Colors, Fonts} from '@assets';
import {scaleSizeWidth, scaleSizeHeight} from '@utils/Scaler';
import {AppText} from '@components';

type Props = {
  disabled?: boolean;
  size?: number;
  lineHeight?: number;
  fontFamily?: string;
  title: string;
  borderWidth?: number;
  borderColor?: string;
  borderRadius?: number;
  backgroundColor?: string;
  paddingVertical?: number;
  onPress: () => void;
  style?: ViewStyle;
  textColor?: string;
  underlayColor?: string;
  flex?: boolean;
  mt?: number;
  mb?: number;

};

const AppButton = ({
  disabled,
  size = 17,
  lineHeight = 20,
  fontFamily = Fonts.w_700,
  title, 
  borderWidth = 0,
  borderColor = Colors.main,
  borderRadius = 44,
  backgroundColor = Colors.main,
  paddingVertical = 17,
  onPress, 
  style, 
  textColor = Colors.white, 
  underlayColor = Colors.main,
  flex,
  mt = 0,
  mb = 0
}:Props) => {
  const styleConfig = StyleSheet.flatten([
    {
      paddingVertical: scaleSizeWidth(paddingVertical - borderWidth),
      borderRadius: scaleSizeWidth(borderRadius),
      backgroundColor,
      borderWidth,
      borderColor,
      flex: flex ? 1 : 0,
      marginTop: scaleSizeHeight(mt),
      marginBottom: scaleSizeHeight(mb)
    },
    disabled && {backgroundColor: Colors.background02},
    style
  ]);
  return (
    <TouchableHighlight 
      style={[styles.container, styleConfig]} 
      onPress={onPress} 
      underlayColor={underlayColor}
      disabled={disabled}
    >
      <AppText size={size} lineHeight={lineHeight} color={textColor} fontFamily={fontFamily}>{title}</AppText>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default React.memo(AppButton);
