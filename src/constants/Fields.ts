import { RefObject, createRef } from "react";
import { StyleSheet, Platform, ViewStyle, View } from 'react-native';
import store from '@store';
import { scaleSizeHeight } from "@utils/Scaler";
import { INPUT_TYPE, MEDIA_TYPE } from "@constants";
import { Icons } from "@assets";

const { lang } = store.getState().global as any;
const refPassword = createRef();

const styles = StyleSheet.create({
    mt20: {
        marginTop: scaleSizeHeight(20)
    },
    mt16: {
        marginTop: scaleSizeHeight(16)
    },
    mt28: {
        marginTop: scaleSizeHeight(28)
    }
});

export type Field = {
    field: string;
    label: string;
    placeHolder?: string;
    isPassword?: boolean;
    keyboardType?: string;
    stylesContainer?: ViewStyle;
    errorMessage?: string;
    returnKeyType?: string;
    ref?: RefObject<any>;
    type?: INPUT_TYPE;
    rightIcon?: JSX.Element;
    showRowMessage?: boolean;
    onSubmitEditing?: () => void;
};

export const fieldsTest = [
    {
        field: 'user',
        label: lang.form.phone,
        placeHolder: lang.form.phone_pl,
        keyboardType: 'phone-pad',
        stylesContainer: styles.mt20,
        returnKeyType: (Platform.OS === 'ios') ? 'done' : 'next',
        onSubmitEditing: () => (refPassword.current as any)?.focus(),
    },
    {
        field: 'password',
        label: lang.form.password,
        placeHolder: lang.form.password_pl,
        isPassword: true,
        stylesContainer: styles.mt20,
        ref: refPassword,
        returnKeyType: (Platform.OS === 'ios') ? 'done' : 'next',
    },
    {
        field: 'image',
        label: lang.form.my_picture_pet,
        stylesContainer: styles.mt16,
        type: INPUT_TYPE.IMAGE_PICKER,
        mediaType: MEDIA_TYPE.MIXED
    }
] as Field[];

export const fieldsLogin = [
    {
        field: 'user',
        label: lang.form.phone,
        placeHolder: lang.form.phone_pl,
        keyboardType: 'phone-pad',
        stylesContainer: styles.mt20,
        returnKeyType: (Platform.OS === 'ios') ? 'done' : 'next',
        onSubmitEditing: () => (refPassword.current as any)?.focus(),
    },
    {
        field: 'password',
        label: lang.form.password,
        placeHolder: lang.form.password_pl,
        isPassword: true,
        stylesContainer: styles.mt20,
        ref: refPassword,
        returnKeyType: (Platform.OS === 'ios') ? 'done' : 'next',
    }
] as Field[];