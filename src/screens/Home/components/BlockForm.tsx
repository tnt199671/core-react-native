import React, { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import { AppButton, AppInput } from '@components';
import { scaleSizeWidth } from '@utils/Scaler';
import { showToast } from '@components/Toast';
import { TOAST_TYPE } from '@constants';
import { Resolver, useForm } from 'react-hook-form';
import { FORM_TEST_VALIDATION } from '@utils/Validation';
import { yupResolver } from '@hookform/resolvers/yup';
import { fieldsTest } from '@constants/Fields';
import { RootState } from '@store';
import { useSelector } from 'react-redux';

type BlockFormParams = {
  user: string;
  password: string;
  image: string;
};

const BlockForm = () => {
  const { lang } = useSelector((state: RootState) => state.global) as any;
  const {
    handleSubmit,
    formState: { errors },
    control,
    watch,
    reset
  } = useForm<BlockFormParams>({
    mode: "onSubmit",
    defaultValues: {
      user: '',
      password: '',
      image: ''
    },
    resolver: yupResolver(FORM_TEST_VALIDATION) as Resolver<BlockFormParams>,
  });

  const onSubmit = useCallback((data: BlockFormParams) => {
    showToast(TOAST_TYPE.SUCCESS, 'test nè :D', 'haha');
  }, []);

  return (
    <View style={styles.container}>
      {fieldsTest?.map((item, key) => (
        <AppInput
          onSubmitEditing={handleSubmit(onSubmit)}
          {...item}
          error={errors[item.field as keyof typeof errors]?.message}
          key={key}
          control={control}
        />
      ))}
      <AppButton 
        mt={40} 
        title={lang.button.login} 
        onPress={handleSubmit(onSubmit)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: scaleSizeWidth(10),
  }
});

export default React.memo(BlockForm);
