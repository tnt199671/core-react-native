import React, {useEffect, useCallback, useState} from 'react';
import {View, StyleSheet } from 'react-native';
import {Colors, Fonts} from '@assets';
import {useDispatch, useSelector} from 'react-redux';
import {useForm} from 'react-hook-form';
import {yupResolver} from '@hookform/resolvers/yup';
import {scaleSizeWidth, scaleSizeHeight} from '@utils/Scaler';
import {LOGIN_VALIDATION} from '@utils/Validation';
import {loginAction} from '@store/actions/auth';
import {DismissKeyboard, AppHeader, AppText, AppInput, AppButton} from '@components';
import { RootState } from '@store';
import { fieldsLogin } from '@constants/Fields';

const Login = () => {
  const dispatch = useDispatch();
  const { lang } = useSelector((state:RootState) => state.global) as any;
  const [disabledButton, setDisabledButton] = useState(true);

  const {
    handleSubmit,
    formState: { errors },
    control,
    watch,
    reset
  } = useForm<LoginActionParams>({
    mode: "onSubmit",
    defaultValues: {
      user: '',
      password: '',
    },
    resolver: yupResolver(LOGIN_VALIDATION),
  });

  useEffect(() => {
    const subscription = watch((value, { name, type }) =>
      setDisabledButton((value?.user && value?.password) ? false : true)
    )
    return () => subscription.unsubscribe()
  }, [watch]);
   
  const onSubmit = useCallback((data: LoginActionParams) => {
    dispatch(loginAction(data));
  }, []);

  return (
    <DismissKeyboard>
      <View style={styles.wrapper}>
        <AppHeader isLogo/>
        <View style={styles.container}>
          <AppText size={28} lineHeight={35} fontFamily={Fonts.w_700} mb={14}>{lang.title.login}</AppText>
          {fieldsLogin?.map((item, key) => (
            <AppInput 
              onSubmitEditing={handleSubmit(onSubmit)}
              {...item} 
              error={errors[item.field as keyof typeof errors]?.message}
              key={key}
              control={control}
            />
          ))}
          <AppButton 
            mt={40} 
            title={lang.button.login} 
            disabled={disabledButton}
            onPress={handleSubmit(onSubmit)}
          />
        </View>
      </View>
    </DismissKeyboard>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: Colors.white,
    height: '100%',
  },
  container: {
    marginHorizontal: scaleSizeWidth(37),
    marginTop: scaleSizeHeight(30)
  },
  mt20: {
    marginTop: scaleSizeHeight(20)
  }
});

export default React.memo(Login);
