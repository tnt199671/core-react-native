type SheetManagerPayloadParams = {
    payload: {
        type: string;
        data: any;
        callback: any;
    };
};