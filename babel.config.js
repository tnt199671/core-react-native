module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        cwd: 'babelrc',
        root: ['./src'],
        extensions: ['.js', '.ios.js', '.android.js'],
        alias: {
          '@screens': './src/screens',
          '@components': './src/components',
          '@assets': './src/assets',
          '@store': './src/store',
          '@services': './src/services',
          '@navigator': './src/navigator',
          '@constants': './src/constants',
          '@types': './src/types',
          '@lang': './src/lang',
          '@utils': './src/utils',
          '@reactotronConfig': './src/reactotronConfig'
        },
      },
    ],
    'react-native-reanimated/plugin',
    '@babel/plugin-syntax-bigint'
  ],
};
