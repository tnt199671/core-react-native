import client from './clientMessage';

export const apiSendMessage = (params) => (
  client.post(`/send`, params).then(res => res)
);