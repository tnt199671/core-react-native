import React, {  } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Colors, Fonts, Icons } from '@assets';
import { scaleSizeWidth, scaleSizeHeight } from '@utils/Scaler';
import { AppText, AppButton } from '@components';

type DefaultProps = {
  onClose: () => void;
  callback: (value: any) => void;
  data: {
    title: string;
    content: string;
  } | null;
  type?: string;
  visible?: boolean;
};

const Default = ({ onClose, callback, data }: DefaultProps) => {
  return (
    <View style={styles.container}>
      <AppText size={17} lineHeight={20} fontFamily={Fonts.w_700} textAlign={'center'}>
        {data?.title}
      </AppText>
      <AppText size={16} lineHeight={23} mt={11} textAlign={'center'}>
        {data?.content}
      </AppText>
      <TouchableOpacity style={styles.close} onPress={onClose}>
        <Icons.CLOSE_BASE />
      </TouchableOpacity>
      <AppButton
        title={'OK'}
        onPress={onClose}
        style={styles.button}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    marginHorizontal: scaleSizeWidth(24),
    padding: scaleSizeWidth(24),
    borderRadius: scaleSizeWidth(18)
  },
  close: {
    position: 'absolute',
    right: scaleSizeWidth(13),
    top: scaleSizeHeight(10)
  },
  button: {
    marginTop: scaleSizeHeight(20),
    marginHorizontal: scaleSizeWidth(70)
  }
});

export default React.memo(Default);
