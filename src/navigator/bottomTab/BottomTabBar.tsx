import React, {useCallback} from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Platform,
  SafeAreaView
} from 'react-native';
import {Colors, Fonts} from '@assets';
import {AppText} from '@components';
import {scaleSizeHeight} from '@utils/Scaler';

type RouteParams = {
  key: string;
  name: string;
  params?: any;
};

type BottomTabBarProps = {
  state: any;
  descriptors: any;
  navigation: any;
};

type TabProps = {
  route: RouteParams;
  index: number;
  state: any;
  descriptors: any;
  navigation: any;
};

const Tab = ({route, index, state, descriptors, navigation}: TabProps) => {
  const {options} = descriptors[route.key];
  const isFocused = state.index === index;

  const onPress = useCallback(() => {
    const event = navigation.emit({
      type: 'tabPress',
      data: {key: route.key},
      canPreventDefault: true,
    });

    if (!isFocused) {
      navigation.navigate(route.name);
    } else if (isFocused) {
      navigation.emit({
        type: 'focusTabPress',
        target: route.key,
      });
    }
  }, [navigation, route, isFocused, state.index, index]);

  return (
    <TouchableOpacity style={styles.tab} onPress={onPress} activeOpacity={1}>
      <options.icon size={24} isActive={isFocused}/>
      <AppText 
        size={12} 
        mt={8} 
        ml={15}
        mr={15}
        textAlign={'center'}
        fontFamily={isFocused ? Fonts.w_600 : Fonts.w_400}
      >{options.title}</AppText>
    </TouchableOpacity>
  );
};

const BottomTabBar = ({state, descriptors, navigation}: BottomTabBarProps) => {
  return(
    <SafeAreaView style={styles.container}>
      {state.routes.map((route: RouteParams, index: number) => (
        <Tab
          key={route.key}
          route={route}
          index={index}
          state={state}
          descriptors={descriptors}
          navigation={navigation}
        />
      ))}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: Colors.white,
    ...Platform.select({
      android: {
        elevation: 9,
        borderTopColor: '#F6F6F7',
        borderTopWidth: 2,
        paddingBottom: scaleSizeHeight(20),
      },
      ios: {
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: -4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,
      },
    }),
  },
  tab: {
    flex: 1,
    backgroundColor: Colors.white,
    alignItems: 'center',
    paddingTop: scaleSizeHeight(15)
  }
});

export default React.memo(BottomTabBar);
