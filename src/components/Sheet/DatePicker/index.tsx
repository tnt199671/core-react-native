import React, { useCallback, useState } from 'react';
import { View, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { Colors, Fonts, Icons } from '@assets';
import { scaleSizeWidth, scaleSizeHeight } from '@utils/Scaler';
import DatePicker from "react-native-date-picker";
import { AppText, AppButton } from '@components';
import { RootState } from '@store';

type DatePickerSheetProps = {
  data: {
    title: string;
    value: string;
  } | null;
  callback: (value: any) => void;
  onHide: () => void;
};

const DatePickerSheet = ({ data, callback, onHide }: DatePickerSheetProps) => {
  const { lang } = useSelector((state: RootState) => state.global) as any;
  const [date, setDate] = useState(data?.value ? new Date(data?.value) : new Date());

  const onSubmit = useCallback(() => {
    onHide();
    callback(date);
  }, [date, callback, onHide])

  return (
    <View style={styles.container}>
      <View style={styles.wrapperHeader}>
        <AppText size={17} lineHeight={24} fontFamily={Fonts.w_600}>
          {data?.title}
        </AppText>
        <TouchableOpacity onPress={onHide}>
          <Icons.CLOSE_BASE />
        </TouchableOpacity>
      </View>
      <DatePicker
        date={date}
        onDateChange={setDate}
        mode="date"
        locale={lang._language}
        style={styles.datePicker}
        maximumDate={new Date()}
        timeZoneOffsetInMinutes={0}
      />
      <AppButton
        title={'OK'}
        style={styles.button}
        onPress={onSubmit}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    paddingTop: scaleSizeHeight(14),
    paddingBottom: Platform.OS === 'android' ? scaleSizeHeight(24) : 0
  },
  wrapperHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: scaleSizeWidth(24),
  },
  datePicker: {
    marginTop: scaleSizeHeight(47),
    alignSelf: "center",
  },
  button: {
    marginHorizontal: scaleSizeWidth(43),
    marginTop: scaleSizeHeight(59)
  }
});

export default React.memo(DatePickerSheet);
