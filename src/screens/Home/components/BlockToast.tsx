import React, { useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { AppButton } from '@components';
import { scaleSizeWidth } from '@utils/Scaler';
import { showToast } from '@components/Toast';
import { TOAST_TYPE } from '@constants';

const BlockToast = () => {

  const onPress = useCallback(() => {
    showToast(TOAST_TYPE.ERROR, 'test nè :D', 'haha');
  }, []);

  return (
    <AppButton
      title={'Test Toast'}
      onPress={onPress}
      style={styles.container}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: scaleSizeWidth(10),
  }
});

export default React.memo(BlockToast);
