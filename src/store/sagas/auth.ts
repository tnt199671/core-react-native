import {put, call, takeLatest, select, race} from 'redux-saga/effects';
import {
  LOGIN,
  LOGOUT,
  updateAuthStoreAction
} from '@store/actions/auth';
import { showLoadingAction, appMountedAction } from '@store/actions/global';
import NavigationService from '@services/navigation';
import { ScreenNames } from '@navigator/ScreenNames';
import messaging from '@react-native-firebase/messaging';

const getFcmToken = async () => {
  return await messaging().getToken();
}

type LoginSagaParams = {
  params: LoginActionParams;
  type: string;
};

export function* login({params, type}: LoginSagaParams): any {
  try {
    yield put(showLoadingAction(true));
    const fcmTokenCurrent = yield call(getFcmToken);
    console.log(fcmTokenCurrent);
    
    yield put(updateAuthStoreAction({
      token: 'abcxyz',
      user: {
        ...params
      },
      fcmToken: fcmTokenCurrent
    }));
    yield put(appMountedAction());
  } catch (e) {
    
  } finally {
    yield put(showLoadingAction(false));
  }
}

export function* logout(): any {
  try {
    yield put(showLoadingAction(true));
    yield put(updateAuthStoreAction({
      token: undefined,
      user: undefined
    }));
    NavigationService.reset(ScreenNames.Login);
  } catch (e) {
    
  } finally {
    yield put(showLoadingAction(false));
  }
}

function* AuthSaga() {
  yield takeLatest(LOGIN, login);
  yield takeLatest(LOGOUT, logout);
}

export default AuthSaga;