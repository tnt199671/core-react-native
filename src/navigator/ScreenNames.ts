export enum ScreenNames {
  Home = 'Home',
  Gift = 'Gift',
  Scan = 'Scan',
  Tip = 'Tip',
  Shop = 'Shop',
  BottomTabBar = 'BottomTabBar',

  Login = 'Login',
}