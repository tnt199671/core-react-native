
import React, {useRef} from 'react';
import DropdownAlert, {
  DropdownAlertData,
} from 'react-native-dropdownalert';

interface ToastRef {
  show: (type: string, message: string, title: string) => void;
};

let globalRef: React.MutableRefObject<ToastRef | null> = useRef(null);

export const showToast = (type: string, message: string, title: string) => {
  if (globalRef.current) {
    globalRef.current.show(type, message, title);
  }
};

const Toast = () => {
  let alert = useRef((_data?: DropdownAlertData) => new Promise<DropdownAlertData>(res => res));
  let dismiss = useRef(() => {});

  React.useImperativeHandle(globalRef, () => ({
    show: (type: string, message: string, title: string) => onShow(type, message, title),
  }));

  const onShow = async (type: string, message: string, title: string) => {
    const alertData = {
      type,
      title,
      message,
      interval: 3000,
    };
    await alert.current(alertData);
  }

  return (
    <DropdownAlert
      alert={func => (alert.current = func)}
      dismiss={func => (dismiss.current = func)}
    />
  );
};

export default Toast;
