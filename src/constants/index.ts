import { DropdownAlertType } from "react-native-dropdownalert";

export enum POPUP_TYPE {
  DEFAULT = 'DEFAULT',
};

export enum SHEET_TYPE {
  DEFAULT = 'DEFAULT',
  DATE_PICKER = 'DATE_PICKER',
  ANY = 'ANY'
};

export enum SHEET {
  DEFAULT = 'DEFAULT',
  NOT_HEADER = 'NOT_HEADER',
};

export enum THEME_MODE {
  LIGHT = 'LIGHT',
  DARK = 'DARK'
};

export enum ORIENTATION {
  PORTRAIT = 'PORTRAIT',
  LANDSCAPE_RIGHT = 'LANDSCAPE-RIGHT',
  PORTRAIT_UPSIDEDOWN = 'PORTRAIT-UPSIDEDOWN',
  LANDSCAPE_LEFT = 'LANDSCAPE-LEFT',
  FACE_UP = 'FACE-UP'
};

export enum APP_STATE {
  ACTIVE = 'active',
  INACTIVE = 'inactive',
  BACKGROUND = 'background'
};

export enum INPUT_TYPE {
  DEFAULT = 'DEFAULT',
  DATE_PICKER = 'DATE_PICKER',
  IMAGE_PICKER = 'IMAGE_PICKER'
};

export enum LANG {
  VI = 'vi',
  EN = 'en'
};

export enum STATUS_CODE {
  SUCCESS = 200,
  BAD_RQ = 400,
  CUSTOM = 406
};

export enum TOAST_TYPE {
    INFO = DropdownAlertType.Info,
    WARN = DropdownAlertType.Warn,
    ERROR = DropdownAlertType.Error,
    SUCCESS = DropdownAlertType.Success,
};

export enum MEDIA_TYPE {
  LIBRARY = 'LIBRARY',
  CAMERA = 'CAMERA',
  MIXED = 'MIXED'
};