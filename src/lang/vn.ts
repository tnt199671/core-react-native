export default {
    form: {
        phone: 'Số điện thoại',
        phone_pl: 'Nhập số điện thoại (Bắt buộc)',
        password: 'Mật khẩu',
        password_pl: 'Nhập mật khẩu (Bắt buộc)',
    },
    button: {
        login: 'Đăng nhập',
        signup: 'Đăng ký',
        logout: 'Đăng xuất',
    },
    sheet: {

    },
    popup: {

    },
    title: {

    },
    text: {

    },
    toast: {

    }
};
