import React, {useCallback, useState, forwardRef, useMemo} from 'react';
import {TextInput, View, TouchableOpacity, TouchableWithoutFeedback, StyleSheet, TextStyle} from 'react-native';
import {Colors, Fonts, Icons} from '@assets';
import {scaleSizeWidth, scaleSizeHeight} from '@utils/Scaler';
import {INPUT_TYPE} from '@constants';
import moment from 'moment';
import {AppText} from '@components';
import { useSelector } from 'react-redux';
import { RootState } from '@store';

type InputProps = {
  label: string;
  value: string;
  onChange: (text: string) => void;
  placeHolder?: string;
  isPassword?: boolean;
  rightIcon?: React.ReactNode;
  error?: string;
  errorMessage?: string;
  isSelect?: boolean;
  disabled?: boolean;
  type?: string;
  style?: TextStyle;
  onPress: (onChange: (value: any) => void, value: string) => void;
};

type RenderRightIconProps = {
  isPassword?: boolean;
  showPass?: boolean;
  rightIcon: React.ReactNode;
  onPress: () => void;
}

const RenderRightIcon = ({isPassword, showPass, rightIcon, onPress}: RenderRightIconProps) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={1}>
      {isPassword && <Icons.EYES isOpen={showPass}/>}
      {rightIcon && rightIcon}
    </TouchableOpacity>
  )
}

const Input = ({
  label,
  value,
  onChange,
  placeHolder,
  isPassword,
  rightIcon,
  error,
  errorMessage,
  isSelect,
  disabled,
  type,
  onPress,
  ...props
}: InputProps, ref: React.LegacyRef<TextInput> | undefined) => {
  const { lang } = useSelector((state: RootState) => state.global) as any;
  const [focused, setFocued] = useState(false);
  const [showPass, setShowpass] = useState(isPassword);

  const onPressRight = useCallback(() => {
    if(disabled) return;
    if(isPassword){
      return setShowpass(!showPass);
    }
    if(isSelect){
      onPress(onChange, value);
    }
  }, [isPassword, showPass, disabled, isSelect, value, onChange]);

  const onFocus = useCallback(() => {
    setFocued(true);
  }, []);

  const onBlur = useCallback(() => {
    setFocued(false);
  }, []);

  const newValue = useMemo(() => {
    if(type === INPUT_TYPE.DATE_PICKER){
      return value ? moment(value).utc().format('DD/MM/YYYY') : '';
    }
    return value;
  }, [value])

  return (
    <View>
      <AppText size={13} lineHeight={24} fontFamily={Fonts.w_700} color={Colors.text01}>{label}</AppText>
      <TouchableWithoutFeedback 
        onPress={isSelect ? onPressRight : () => null}
      >
        <View style={[
          styles.container,
          focused && {borderColor: Colors.black},
          !!error && {borderColor: Colors.border02}
        ]}>
          <TextInput
            ref={ref}
            {...props}
            onFocus={onFocus}
            onBlur={onBlur}
            secureTextEntry={isPassword && showPass}
            placeholder={placeHolder}
            style={[styles.inputStyle, props.style]}
            onChangeText={onChange}
            value={newValue || ""}
            editable={(isSelect || disabled) ? false : true}
            placeholderTextColor={Colors.text02}
            pointerEvents={(isSelect || disabled) ? 'none' : 'auto'}
            textContentType={'oneTimeCode'}
          />
          <RenderRightIcon 
            isPassword={isPassword}
            showPass={showPass}
            onPress={onPressRight}
            rightIcon={rightIcon}
          />
        </View>
      </TouchableWithoutFeedback>
      { error &&
        <AppText mt={7} color={Colors.main}>{error}</AppText>
      }
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: scaleSizeWidth(1),
    borderColor: Colors.border01,
    height: scaleSizeWidth(48),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputStyle: {
    flex: 1,
    fontSize: scaleSizeWidth(17),
    color: Colors.black,
    fontFamily: Fonts.w_400
  },
  rowMessage: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: scaleSizeHeight(12)
  }
});

export default forwardRef(Input);
