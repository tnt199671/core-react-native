import React, { useCallback, useState } from 'react';
import { View, StyleSheet, TouchableOpacity, TextInput, ViewStyle } from 'react-native';
import { useDispatch } from 'react-redux';
import { Colors, Fonts, Icons } from '@assets';
import { scaleSizeWidth, scaleSizeHeight } from '@utils/Scaler';
import { debounce } from "lodash";

type AppInputSearchProps = {
  style?: ViewStyle;
  onChange: (text: string) => void;
  time?: number;
  placeholder?: string;
};

const AppInputSearch = ({
  style,
  onChange,
  time = 3000,
  placeholder,
  ...props
}: AppInputSearchProps) => {
  const [text, setText] = useState('');

  function handleDebounceFn(text: string) {
    onChange(text);
  }

  const debounceFn = useCallback(debounce(handleDebounceFn, time), []);

  const onChangeText = useCallback((text: string) => {
    setText(text);
    debounceFn(text);
  }, [debounceFn]);

  const onSubmitEditing = useCallback(() => {
    onChange(text);
    debounceFn.cancel();
  }, [text]);

  return (
    <View style={[styles.container, style]}>
      <Icons.SEARCH />
      <TextInput
        {...props}
        style={styles.inputStyle}
        value={text}
        onChangeText={onChangeText}
        returnKeyType={'search'}
        placeholderTextColor={Colors.text02}
        onSubmitEditing={onSubmitEditing}
      />
      {text?.length > 0 && (
        <TouchableOpacity onPress={() => onChangeText('')}>
          <Icons.CLOSE_GRAY />
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.background07,
    height: scaleSizeHeight(48),
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: scaleSizeWidth(30),
    paddingLeft: scaleSizeWidth(12),
    paddingRight: scaleSizeWidth(15)
  },
  inputStyle: {
    flex: 1,
    fontSize: scaleSizeWidth(14),
    lineHeight: scaleSizeHeight(20),
    fontFamily: Fonts.w_400,
    color: Colors.black,
    marginLeft: scaleSizeWidth(12),
    marginRight: scaleSizeWidth(10)
  }
});

export default React.memo(AppInputSearch);
