import {
  SHOW_LOADING,
  UPDATE_GLOBAL_STORE,
  CHANGE_LANGUAGE
} from '../actions/global';
import {ORIENTATION} from '@constants';
import lang from '@lang';

const initState = {
  isLoading: false,
  appState: null,
  orientation: ORIENTATION.PORTRAIT,
  lang: lang
};

const global = (state = initState, action: any) => {
  switch (action.type) {
    case UPDATE_GLOBAL_STORE:
      return {
        ...state,
        ...action.payload
      };
    case SHOW_LOADING:
      return {
        ...state,
        isLoading: action.visible
      };
    case CHANGE_LANGUAGE:
      lang.setLanguage(action.language);
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default global;
