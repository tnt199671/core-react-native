import Colors from './res/color';
import Fonts from './res/font';
import Images from './res/image';
import Animations from './res/animation';
import Icons from './res/icon';
export {
	Colors, 
	Fonts, 
	Images, 
	Animations,
	Icons
};