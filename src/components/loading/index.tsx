import React from 'react';
import {Modal, View, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import LottieView from 'lottie-react-native';
import {Animations, Colors} from '@assets';
import {scaleSizeWidth} from '@utils/Scaler';
import { RootState } from '@store';

const Loading = () => {
  const { isLoading } = useSelector((state: RootState) => state.global) as any;
  return (
    <Modal
      transparent={true}
      visible={isLoading}
      supportedOrientations={['portrait', 'landscape']}
      statusBarTranslucent
    >
      <View style={styles.containLoading}>
        <LottieView
          style={styles.lottieView}
          source={Animations.loading}
          autoPlay
          loop
          duration={2000}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  containLoading: {
    backgroundColor: Colors.background03,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  lottieView: {
    width: scaleSizeWidth(200),
    height: scaleSizeWidth(200),
  },
});

export default React.memo(Loading);
