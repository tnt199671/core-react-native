import React, {useCallback} from 'react';
import {TouchableWithoutFeedback, Keyboard} from 'react-native';

type DismissKeyboardProps = {
  children: React.ReactNode;
};

const DismissKeyboard = ({children}: DismissKeyboardProps) => {
  const onDismissKeyboard = useCallback(() => {
    Keyboard.dismiss();
  }, []);

  return (
    <TouchableWithoutFeedback onPress={onDismissKeyboard}>
      {children}
    </TouchableWithoutFeedback>
  );
};

export default React.memo(DismissKeyboard);
