import React, { ReactNode } from 'react';
import {Text, StyleSheet, TextStyle} from 'react-native';
import {Colors, Fonts} from '@assets';
import {scaleSizeWidth, scaleSizeHeight} from '@utils/Scaler';

type Props = {
  children: ReactNode;
  textAlign?: 'left' | 'right' | 'center';
  flex?: boolean;
  size?: number;
  lineHeight?: number;
  fontFamily?: string;
  color?: string;
  mt?: number;
  mb?: number;
  pt?: number;
  pb?: number;
  ml?: number;
  mr?: number;
  style?: TextStyle;
  onPress?: () => void;
  numberOfLines?: number;
};

const AppText = ({
  children,
  textAlign = 'left',
  flex,
  size = 14,
  lineHeight = 16,
  fontFamily = Fonts.w_400,
  color = Colors.black,
  mt = 0,
  mb = 0,
  pt = 0,
  pb = 0,
  ml = 0,
  mr = 0,
  style,
  ...props
}: Props) => {
  const styleConfig = StyleSheet.flatten([
    {
      textAlign, 
      color, 
      flex: flex ? 1 : 0, 
      fontSize: scaleSizeWidth(size), 
      lineHeight: scaleSizeWidth(lineHeight), 
      fontFamily,
      marginTop: scaleSizeWidth(mt),
      marginBottom: scaleSizeWidth(mb),
      paddingTop: scaleSizeWidth(pt),
      paddingBottom: scaleSizeWidth(pb),
      marginLeft: scaleSizeWidth(ml),
      marginRight: scaleSizeWidth(mr)
    },
    style
  ]);
  return (
    <Text {...props} style={[styleConfig, styles.container]}>
    	{children}
    </Text>
  );
};

const styles = StyleSheet.create({
  container: {
  },
});

export default React.memo(AppText);
