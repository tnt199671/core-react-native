import LocalizedStrings from 'react-native-localization';
import vn from './vn';
import en from './en';

const language = new LocalizedStrings({
  vn,
  en,
});

export default language;
