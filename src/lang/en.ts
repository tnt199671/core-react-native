export default {
    form: {
        phone: 'Phone number',
        phone_pl: 'Enter phone number (required)',
        password: 'Password',
        password_pl: 'Enter password (required)',
    },
    button: {
        login: 'Login',
        signup: 'Sign up',
        logout: 'Sign out',
    },
    sheet: {

    },
    popup: {

    },
    title: {

    },
    text: {

    },
    toast: {

    }
};
